const connect = require("../db/connect");

module.exports = class clienteController {
    static async createCliente(req, res) {
        const {
            telefone,
            nome,
            cpf,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            estado,
            cep,
            referencia
        } = req.body

        if (telefone !== 0) {
            const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values ( 
                '${telefone}', 
                '${nome}', 
                '${cpf}', 
                '${logradouro}', 
                '${numero}', 
                '${complemento}', 
                '${bairro}', 
                '${cidade}', 
                '${estado}', 
                '${cep}', 
                '${referencia}' )`;

            try {
                connect.query(query, function (err) {
                    if (err) {
                        console.log(err);
                        res.status(500).json({ error: "Usuário não cadastrado no banco!!!" });
                        return;
                    }
                    console.log("Inserido no Banco!!!");
                    res.status(201).json({ message: "Usuário criado com sucesso!!!" });
                });
            } catch (error) {
                console.error("error ao executar o insert", error);
                res.status(500).json({ error: "Erro interno do servidor!!!" })
            }

        }
        else {
            res.status(400).json({ message: "O Telefone é obrigatorio!!!" })

        }

    }
    static async getAllClientes(req, res) {
        const query = `select * from cliente`;

        try {
            connect.query(query, function (err, data) {
                if (err) {
                    console.log(err);
                    res.status(500).json({ error: "Usuários não encontrados no Banco" });
                    return;
                }
                let clientes = data;
                console.log("Consulta realizada com sucesso");
                res.status(201).json({ clientes })

            });

        } catch (error) {
            console.error("Erro ao execultar a consulta: ", error);
            res.status(500).json({ error: "Erro interno no servidor" })

        }

    }
    static async getAllClientes2(req, res) {
        const { filtro, ordenacao, ordem } = req.query;

        let query = `select * from cliente`;

        if (filtro) {
            //esta incorreto
            //query = query + filtro; 

            //jeito certo mas não muito comum
            //query = query + `where ${filtro}`; 

            //jeito mais usado
            query += ` where ${filtro}`;

        }

        if(ordenacao){
            query += ` order by ${ordenacao}`;
            if (ordem) {
                query += ` ${ordem}`;
            }

        }

        try {
            connect.query(query, function (err, result) {
                if (err) {
                    console.log(err);
                    res.status(500).json({ error: "Usuários não encontrados no Banco" });
                    return;
                }

                console.log("Consulta realizada com sucesso");
                res.status(201).json({ result })

            });

        } catch (error) {
            console.error("Erro ao execultar a consulta: ", error);
            res.status(500).json({ error: "Erro interno no servidor" })

        }

    }

}