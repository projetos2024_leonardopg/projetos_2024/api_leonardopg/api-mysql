const router = require("express").Router();
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController")

router.get("/table", dbController.getTables);
router.post("/telefone", clienteController.createCliente);
router.get("/cliente", clienteController.getAllClientes);
router.get("/cliente2", clienteController.getAllClientes2)

router.get("/listarPedidosPizza/",pizzariaController.listarPedidosPizzas);

module.exports = router;
